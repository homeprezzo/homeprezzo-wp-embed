(function() {
    tinymce.create("tinymce.plugins.homeprezzo_embed_shortcode", {

        //url argument holds the absolute url of our plugin directory
        init : function(ed, url) {

            //add new button     
            ed.addButton("homeprezzo-embed-shortcode", {
                title : "Embed HomePrezzo Prezzo",
                cmd : "homeprezzo_embed_command",
                image : HomeprezzoEmbed.settings.icon
            });

            //button functionality.
            ed.addCommand("homeprezzo_embed_command", function() {

                var popup = jQuery('<div data-context="homeprezzo-embed-mce-dialog" title="Embed HomePrezzo Prezzo"><div data-context="content"></div></div>');
                
                var content = jQuery(popup.find('[data-context=content]'));

                content.html('Waiting for data ...');

                jQuery.ajax({
                    url: '/index.php?homeprezzo-embed&f=prezzos',
                    method: 'get',
                    success: function(data) {
                        if (data.error) {
                            content.html(data.error);
                        }
                        else {
                            var selector = jQuery('<select data-context="prezzo"></select>');
                            data.prezzos.map(function(prezzo) {
                                if (prezzo.status) {
                                    selector.append(new Option(prezzo.title + ' (' + prezzo.url + ')', prezzo.url, false, false));
                                }
                            });
                            var color = jQuery('<input type="text" />');
                            var submit = jQuery('<button type="button" data-context="submit">Embed Prezzo</button>');

                            content.html('');
                            jQuery('<strong>Prezzo</strong>').appendTo(content);
                            jQuery('<br />').appendTo(content);
                            selector.appendTo(content);
                            jQuery('<br />').appendTo(content);
                            jQuery('<strong>Background</strong>').appendTo(content);
                            jQuery('<br />').appendTo(content);
                            color.appendTo(content);
                            jQuery('<br />').appendTo(content);
                            submit.appendTo(content);
                            color.wpColorPicker();

                            submit.on('click', function(e) {
                                e.preventDefault();
                                var return_text = '[hpe p="' + selector.val() + '"';
                                if (color.val()) {
                                    return_text += ' c="' + color.val() + '"';
                                }
                                return_text += ']';
                                ed.execCommand('mceInsertContent', 0, return_text);
                                popup.dialog('destroy');
                            })
                        }
                    }
                });

                popup.dialog({
                    modal: true,
                    width: 500
                });

            });

        },

        createControl : function(n, cm) {
            return null;
        },

        getInfo : function() {
            return {
                longname : "HomePrezzo Embed",
                author : "HomePrezzo",
                version : "1"
            };
        }
    });

    tinymce.PluginManager.add("homeprezzo_embed_shortcode", tinymce.plugins.homeprezzo_embed_shortcode);
})();
