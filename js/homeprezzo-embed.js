(function (jQuery) {

  var methods = {

    init: function() {

      jQuery(document).ready(function() {

        var dialog = jQuery('[data-context=homeprezzo-embed-connect-dialog]');
        dialog.dialog({
          autoOpen: false,
          modal: true,
          width: 600,
          height: 450
        });

        var authButton = jQuery('[data-context=homeprezzo-embed-connect]');
        authButton.on('click', function(e) {
          dialog.dialog('open');
        });

        var clientLink = jQuery('[data-context=homeprezzo-embed-client-detail]');

        if (clientLink.length === 1) {
          var clientTable = jQuery('#client_id').parent().parent().parent().parent();

          clientTable.hide();

          clientLink.on('click', function(e) {
            e.preventDefault();
            if (clientLink.data('opened')) {
              clientLink.data('opened', false);
              clientTable.hide();
            }
            else {
              clientLink.data('opened', true);
              clientTable.show();
            }
          });
        }

      });

    },

    qtags: function() {
      var popup = jQuery('<div data-context="homeprezzo-embed-mce-dialog" title="Embed HomePrezzo Prezzo"><div data-context="content"></div></div>');
      
      var content = jQuery(popup.find('[data-context=content]'));

      content.html('Waiting for data ...');

      jQuery.ajax({
          url: '/index.php?homeprezzo-embed&f=prezzos',
          method: 'get',
          success: function(data) {
              if (data.error) {
                  content.html(data.error);
              }
              else {
                  var selector = jQuery('<select data-context="prezzo"></select>');
                  data.prezzos.map(function(prezzo) {
                      if (prezzo.status) {
                          selector.append(new Option(prezzo.title + ' (' + prezzo.url + ')', prezzo.url, false, false));
                      }
                  });
                  var color = jQuery('<input type="text" />');
                  var submit = jQuery('<button type="button" data-context="submit">Embed Prezzo</button>');

                  content.html('');
                  jQuery('<strong>Prezzo</strong>').appendTo(content);
                  jQuery('<br />').appendTo(content);
                  selector.appendTo(content);
                  jQuery('<br />').appendTo(content);
                  jQuery('<strong>Background</strong>').appendTo(content);
                  jQuery('<br />').appendTo(content);
                  color.appendTo(content);
                  jQuery('<br />').appendTo(content);
                  submit.appendTo(content);
                  color.wpColorPicker();

                  submit.on('click', function(e) {
                      e.preventDefault();
                      var return_text = '[hpe p="' + selector.val() + '"';
                      if (color.val()) {
                          return_text += ' c="' + color.val() + '"';
                      }
                      return_text += ']';
                      QTags.insertContent(return_text);
                      popup.dialog('destroy');
                  })
              }
          }
      });

      popup.dialog({
          modal: true,
          width: 500
      });
    }

  };

  jQuery.fn.homeprezzoEmbed = function(methodOrOptions) {

    // Determine if supplied is a method string with options, or just options
    if (methods[methodOrOptions]) {
      // If it's a recognised method, then call it with any options
      return methods[methodOrOptions].apply(this,Array.prototype.slice.call(arguments, 1));
    }
    else if (typeof methodOrOptions === 'object' || ! methodOrOptions) {
      // If not, try to apply the options to the init method
      return methods.init.apply(this, arguments);
    }
    else {
      // Otherwise emit an error because the called method doesn't exist
      jQuery.error( 'Method ' +  methodOrOptions + ' does not exist for jQuery.homeprezzoEmbed' );
    }

  }

}(jQuery));

jQuery.fn.homeprezzoEmbed();
