# HomePrezzo Embed

[HomePrezzo Embed](https://bitbucket.org/homeprezzo/homeprezzo-wp-embed/) is a simple plugin which enables you to embed your HomePrezzo Prezzos into your WordPress content.

# Instructions

1. [Download](https://bitbucket.org/homeprezzo/homeprezzo-wp-embed/downloads/) and install the plugin into your WordPress plugins directory, and then activate the plugin.  
2. Once activated, visit Settings > HomePrezzo Embed.  
3. Enter your client ID and client secret.  If you don't have one of these, please contact us at [support@homeprezzo.com](mailto:support@homeprezzo.com).  
4. Save your client ID and client secret, and then connect a valid HomePrezzo user.

Now, when you edit your posts and pages, you will see a HomePrezzo Embed button at the top of your editor.  Using this button, you will be able to embed prezzos that you create using [HomePrezzo](https://homeprezzo.com).
